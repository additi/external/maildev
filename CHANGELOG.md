# Changelog
All notable changes to this project will be documented in this file. See [conventional commits](https://www.conventionalcommits.org/) for commit guidelines.

- - -
## 0.0.31 - 2025-03-05
#### Continuous Integration
- add git add . - (53763f0) - youness.chetoui
- fix helm repo index dir - (4b6606e) - youness.chetoui
- fix yq use env - (2510170) - youness.chetoui
- fix yq use env - (44c2037) - youness.chetoui
- change tag to run on k8s - (0744949) - youness.chetoui
- change tag to run on k8s - (3cd3d69) - youness.chetoui
- fix to release helm chart - (56e9b3a) - youness.chetoui
- fix ci to relase helm chart - (c37e258) - youness.chetoui
- fix ci ton release helm chart on tag - (26c87a6) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (7273332) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update helm release oauth2-proxy to v7.12.2 - (71f5909) - dsi additi.fr
- **(deps)** update docker.io/busybox:latest docker digest to 498a000 - (8125a04) - dsi additi.fr
- **(release_helm)** :bookmark: 0.0.31 [nopipeline] - (466a548) - dsi.of2m.fr
- **(release_helm)** ':bookmark:' test release commit - (f75b03f) - youness.chetoui
- **(release_helm)** :bookmark: test release commit - (d90dbd6) - youness.chetoui

- - -

## 0.0.30 - 2025-03-04
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v1.0.6 - (d867227) - dsi additi.fr

- - -

## 0.0.29 - 2025-02-18
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v1.0.3 - (742adaf) - dsi additi.fr

- - -

## 0.0.28 - 2025-02-04
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (4e0fa71) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (af20762) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (67276db) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v1 - (df084c9) - dsi additi.fr
- **(deps)** update helm release oauth2-proxy to v7.10.2 - (d6944e5) - dsi additi.fr
- **(deps)** update docker.io/busybox:latest docker digest to a5d0ce4 - (293ad79) - dsi additi.fr
- :arrow_up: v0.0.4 - (6b6df06) - youness.chetoui

- - -

## 0.0.27 - 2025-01-13
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (1361eaa) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (5a5827f) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (c843538) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (58cafa0) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update helm release oauth2-proxy to v7.9.2 - (655ac56) - dsi additi.fr
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.75 - (b0ccf26) - dsi additi.fr
- **(deps)** update docker.io/maildev/maildev docker tag to v2.2.1 - (51f212d) - dsi additi.fr
- **(deps)** update docker.io/busybox:latest docker digest to 2919d01 - (64afe6f) - dsi additi.fr
- :arrow_up: v2.2.1 - (dc6ff82) - youness.chetoui

- - -

## 0.0.26 - 2024-12-03
#### Bug Fixes
- fix name of chart - (3917f71) - youness.chetoui

- - -

## 0.0.25 - 2024-12-03
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (9424d44) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (673b1bc) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (6d2e753) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update helm release oauth2-proxy to v7.8.0 - (34d5b48) - dsi additi.fr
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.69 - (8a2b1c8) - dsi additi.fr
- **(deps)** update docker.io/busybox:latest docker digest to db142d4 - (d590018) - dsi additi.fr
- fix urls - (d9a3032) - youness.chetoui
- update release - (80e1478) - youness.chetoui
- revert renovate.json [skip ci] - (848a49a) - younessof2m
- test renovatye [skip ci] - (45e227e) - younessof2m
- test renovate [skip ci] - (c1bfaa7) - younessof2m

- - -

## 0.0.24 - 2024-08-28
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (5aa6132) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (596fafe) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (3871593) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (6b0c110) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (2a12078) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.59 - (6a4399f) - dsi of2m.fr
- **(deps)** update helm release oauth2-proxy to v7.7.12 - (4a1ede6) - dsi of2m.fr
- **(deps)** update docker.io/busybox:latest docker digest to 9ae97d3 - (594ac5d) - dsi of2m.fr

- - -

## 0.0.23 - 2024-08-27
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (1902ccf) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.52 - (9f0adca) - youness.chetoui

- - -

## 0.0.22 - 2024-05-23
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (3bca8db) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (e8759b1) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update helm release oauth2-proxy to v7 - (92c321e) - dsi of2m.fr
- **(deps)** update docker.io/busybox:latest docker digest to 5eef5ed - (06c32b1) - dsi of2m.fr
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.34 - (ede913a) - dsi of2m.fr

- - -

## 0.0.21 - 2024-02-19
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (242ce35) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.32 - (768f56a) - dsi of2m.fr

- - -

## 0.0.20 - 2024-02-19
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (400fd90) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** pin dependencies - (7ac995f) - dsi of2m.fr

- - -

## 0.0.19 - 2024-02-19
#### Miscellaneous Chores
- update renovate file - (accb581) - youness.chetoui

- - -

## 0.0.18 - 2024-02-19
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (76aae87) - dsi.of2m.fr
- **(README.md)** Update documentation [nopipeline] - (24ce49a) - dsi.of2m.fr
#### Miscellaneous Chores
- **(deps)** update dependency additi/external/gitlab-templates-common to v0.0.26 - (00fe96c) - dsi of2m.fr
- **(deps)** update helm release oauth2-proxy to v6.24.1 - (3779323) - dsi of2m.fr
- **(renovate.json)** [ci skip] disable digest docker - (25bce29) - youness.chetoui
- **(renovate.json)** modif conf [ci skip] - (9c4e68f) - youness.chetoui

- - -

## 0.0.17 - 2023-11-13
#### Bug Fixes
- **(values.yaml)** :bug: Fix values ressources for redis - (702eff4) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (6e9738e) - dsi.of2m.fr

- - -

## 0.0.16 - 2023-11-13
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (29167bc) - dsi.of2m.fr
#### Features
- **(values.yaml)** :sparkles: Add default ressources for oaut2-proxy and redis - (9796cb8) - youness.chetoui

- - -

## 0.0.15 - 2023-11-13
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (15bbf36) - dsi.of2m.fr
- :memo: Some change in readme template - (2ab01c9) - youness.chetoui

- - -

## 0.0.14 - 2023-11-13
#### Features
- **(oauth2-proxy)** :sparkles: Add oauth2-proxy chart - (da63182) - youness.chetoui

- - -

## 0.0.13 - 2023-11-13
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (d1df23e) - dsi.of2m.fr
#### Features
- **(Chart.yaml)** :sparkles: Add oaut2-proxy to use keycloak - (5c146a6) - youness.chetoui

- - -

## 0.0.12 - 2023-11-13
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (4743b0c) - dsi.of2m.fr
- **(README.md.gotmpl)** :memo: Add link of ingress-nginx-controller docs - (e387e6d) - youness.chetoui
- **(README.md.gotmpl)** :memo: Add instruction to expose SMTP service with ingress controller - (5af266d) - youness.chetoui

- - -

## 0.0.11 - 2023-11-10
#### Features
- **(cronjob.yaml)** :sparkles: Remove reload mail cronjobs - (54f4a4f) - youness.chetoui

- - -

## 0.0.10 - 2023-11-10
#### Bug Fixes
- **(cronjob.yaml)** :bug: Fix cronjobs security context - (c431629) - youness.chetoui

- - -

## 0.0.9 - 2023-11-10
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (01f4510) - dsi.of2m.fr
#### Miscellaneous Chores
- Make some docs and update for helpers.tpl - (9cf6ee3) - youness.chetoui

- - -

## 0.0.8 - 2023-11-10
#### Bug Fixes
- **(values.yaml)** :bug: Fix podSecurityContext - (edb2ef8) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (e24e5d1) - dsi.of2m.fr
- Test docs - (7a86ec7) - youness.chetoui

- - -

## 0.0.7 - 2023-11-10
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (8905176) - dsi.of2m.fr

- - -

## 0.0.6 - 2023-11-10
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (5ae767a) - dsi.of2m.fr
#### Features
- Fix securtity context - (7ae2e1a) - youness.chetoui
- Run as user 1001 - (51a6a1e) - youness.chetoui

- - -

## 0.0.5 - 2023-11-10
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (9b2bd7c) - dsi.of2m.fr
#### Features
- **(values.yaml)** :sparkles: Modification conf par defaut security - (1e6ad9e) - youness.chetoui

- - -

## 0.0.4 - 2023-11-10
#### Documentation
- **(Chart.yaml)** :memo: Changement description chart - (c52512c) - youness.chetoui
- **(README.md)** Update documentation [nopipeline] - (daf5202) - dsi.of2m.fr
- :memo: Modification description - (4317d7f) - youness.chetoui

- - -

## 0.0.3 - 2023-11-10
#### Bug Fixes
- **(cronjob.yaml)** :bug: Fix la recuperation du port du service web - (2f28e33) - youness.chetoui

- - -

## 0.0.2 - 2023-11-10
#### Bug Fixes
- **(values.yaml)** :bug: Suppression ingress smtp - (43e37ca) - youness.chetoui
- :bug: Suppression values dans la release 0.0.1 - (840b2dc) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (72b5384) - dsi.of2m.fr

- - -

## 0.0.1 - 2023-11-10
#### Continuous Integration
- **(.gitlab-ci.yml)** :construction_worker: Modification CI - (be2959b) - youness.chetoui
#### Documentation
- **(README.md)** Update documentation [nopipeline] - (4a59e4d) - dsi.of2m.fr
#### Features
- :sparkles: Ajout des fichiers CI - (8e1bc26) - youness.chetoui
- :sparkles: Ajout du chart maildev - (358ad41) - youness.chetoui

- - -

Changelog generated by [cocogitto](https://github.com/cocogitto/cocogitto).